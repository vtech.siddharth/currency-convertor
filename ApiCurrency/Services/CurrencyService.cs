using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http.Headers;
using System.Text.Json;
using ApiCurrency.Models;

namespace ApiCurrency.Services
{
    public class CurrencyService
    {

        private readonly IHttpClientFactory _clientFactory;

        public CurrencyResponse currencyResponses { get; set; }
        public CurrencyService(IHttpClientFactory clientFactory)
        {
            this._clientFactory = clientFactory;
        }

        public async Task<CurrencyResponse> GetCurrency(string fromCurrency)
        {
            string apiUrl = "https://api.ratesapi.io/api/latest?base=" + fromCurrency;
            var request = new HttpRequestMessage(HttpMethod.Get, apiUrl);
            var client = _clientFactory.CreateClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var response = await client.SendAsync(request);

            if (response.IsSuccessStatusCode)
            {
                var responseStream = await response.Content.ReadAsStreamAsync();
                currencyResponses = await JsonSerializer.DeserializeAsync<CurrencyResponse>(responseStream);
            }
            else
            {
                currencyResponses = new CurrencyResponse();
            }
            return currencyResponses;
        }

        [HttpPost]
        public async Task<List<ConvertedCurrency>> GetConvertedValue(List<Currency> paise)
        {
            List<ConvertedCurrency> result = new List<ConvertedCurrency>();
            foreach (Currency rupee in paise)
            {
                CurrencyResponse money = await GetCurrency(rupee.fromCurrency);
                string currency = rupee.toCurrency;
                double value = 0.0;
                switch (rupee.toCurrency)
                {
                    case "PHP":
                        value = money.rates.PHP;
                        break;
                    case "USD":
                        value = money.rates.USD;
                        break;
                    case "GBP":
                        value = money.rates.GBP;
                        break;
                    case "HKD":
                        value = money.rates.HKD;
                        break;
                    case "IDR":
                        value = money.rates.IDR;
                        break;
                    case "ILS":
                        value = money.rates.ILS;
                        break;
                    default:
                        break;
                }
                ConvertedCurrency Cash = new ConvertedCurrency();
                Cash.convertedAmount = rupee.amount * value;
                Cash.amount = rupee.amount;
                Cash.toCurrency = rupee.toCurrency;
                Cash.fromCurrency = rupee.fromCurrency;

                result.Add((Cash));

            }
            return result;
        }


    }

}
