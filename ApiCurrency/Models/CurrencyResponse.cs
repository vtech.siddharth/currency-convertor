using System;

namespace ApiCurrency.Models
{
    public class CurrencyResponse
    {
         public string @base { get; set; }
        public Rates rates { get; set; }
        public string date { get; set; }
    }
}
