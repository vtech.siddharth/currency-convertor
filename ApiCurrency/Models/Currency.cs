using System;

namespace ApiCurrency.Models
{
    public class Currency
    {
         public double amount {get; set;}

         public string fromCurrency {get; set;}

         public string toCurrency {get; set;}
          public Currency(double amount,string fromCurrency,string toCurrency)
    {
        this.amount=amount;
        this.fromCurrency=fromCurrency;
        this.toCurrency=toCurrency;
    }
    }
}
