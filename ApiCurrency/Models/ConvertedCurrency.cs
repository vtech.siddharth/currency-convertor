using System;

namespace ApiCurrency.Models
{
    public class ConvertedCurrency
    {
         public double amount {get; set;}

         public string fromCurrency {get; set;}

         public string toCurrency {get; set;}

         public double convertedAmount {get; set;}

    }
}
