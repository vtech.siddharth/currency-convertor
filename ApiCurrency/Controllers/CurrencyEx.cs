using System.Collections.Generic;
using ApiCurrency.Models;
using ApiCurrency.Services;
using Microsoft.AspNetCore.Mvc;
using ApiCurrency.Logger;
using Newtonsoft.Json;

namespace ApiCurrency.Controllers
{
    [ApiController]
    [Route("[controller]")]

    public class CurrencyExController : ControllerBase
    {   
        FileLogger _fileLogger =new FileLogger(); 
        InMemory _inMemory=new InMemory();
        CurrencyService _currencyService;
        public CurrencyExController(CurrencyService currencyService)
        {
            this._currencyService = currencyService;
        }


        [HttpPost]
        public List<ConvertedCurrency> GetAmount(List<Currency> currency)
        {    
            var jsonRequest =JsonConvert.SerializeObject(currency);
           
            List<ConvertedCurrency> currencies = _currencyService.GetConvertedValue(currency).Result;
             var jsonResponse =JsonConvert.SerializeObject(currencies);
            Log _log =new Log();
            _log.request= jsonRequest;
            _log.response=jsonResponse;

            _fileLogger.LogData(_log);
            _inMemory.LogData(_log);
            return currencies;
        }

    }
}

