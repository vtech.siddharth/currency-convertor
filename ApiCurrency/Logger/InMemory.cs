using System;
using System.Collections.Generic;

namespace ApiCurrency.Logger
{
    public class InMemory : ILogger
    {
        private Dictionary<long, Log> dataDict=new Dictionary<long, Log>();
        public void LogData(Log log)
        {
            var currentTicks=DateTime.Now.Ticks;
            dataDict.Add(currentTicks, log);
        }
    }
}
