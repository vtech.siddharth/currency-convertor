using System;

namespace ApiCurrency.Logger
{
    public interface ILogger
    {

        public void LogData(Log log);
        
    }
}
