using System;
using System.IO;

namespace ApiCurrency.Logger
{
    public class FileLogger : ILogger
    {
        public void LogData(Log log)
        {
            var currentTicks=DateTime.Now.Ticks;
            File.WriteAllText($"request.{currentTicks}.json",log.request);
            File.WriteAllText($"response.{currentTicks}.json",log.response);
        }
    }
}
