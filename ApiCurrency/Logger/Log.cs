using System;

namespace ApiCurrency.Logger
{
    public class Log
    {
        public string request { get; set; }
        public string response { get; set; }
    }
}
