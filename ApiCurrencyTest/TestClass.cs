﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using ApiCurrency.Models;
using ApiCurrency.Services;
using Xunit;

namespace ApiCurrencyTest
{
    public class TestClass
    {
        

        [Fact]
        public async void GetList_Of_ConvetrtedCurrency()
        {
            List<Currency> amount = new List<Currency>();
            
            Currency currency = new Currency(100, "INR", "USD");
            amount.Add(currency);
            CurrencyService service = new CurrencyService(null);
            var result = await service.GetConvertedValue(amount);
            Assert.NotNull(result);
            Assert.Equal(amount.Count, result.Count);
        }
    }
}
